<?php

class SS_EDD_SL_Updater {

	private $remote_api_url;
	private $item_name;
	private $license;
	private $version;
	private $author;
	private $mode;
	private $title;
	private $field_name;
	private $description;
	private $file;
	private $default_license;


	function __construct( $args = array() ) {

		$args = wp_parse_args( $args, array(
			'remote_api_url'  => 'http://shoestrap.org',
			'item_name'       => '',
			'license'         => '',
			'version'         => '',
			'author'          => 'Aristeides Stathopoulos',
			'mode'            => 'plugin',
			'title'           => '',
			'field_name'      => 'field_name',
			'description'     => 'description',
			'file'            => 'file',
			'default_license' => null
		) );

		extract( $args );

		$this->license         = $license;
		$this->item_name       = $item_name;
		$this->version         = $version;
		$this->author          = $author;
		$this->remote_api_url  = $remote_api_url;
		$this->mode            = $mode;
		$this->title           = $title;
		$this->field_name      = $field_name;
		$this->description     = $description;
		$this->file            = $file;
		$this->default_license = $default_license;


		add_action( 'admin_init', array( $this, 'setup_updater' ) );


		// Activates licenses
		add_action( 'admin_init', array( $this, 'activate_license' ) );

		// Deactivates a theme license when we switch themes
		if ( $this->mode == 'theme' ) {
			add_action( 'switch_theme', array( $this, 'deactivate_license' ) );
		}

		// Deactivates a plugin license when the plugin is de-activated
		if ( $this->mode == 'plugin' ) {
			register_deactivation_hook( $this->file, array( $this, 'deactivate_license' ) );
		}

		add_filter( 'shoestrap_licensing_options_modifier', array( $this, 'options' ) );

	}


	function options( $options ) {

		// Build the text field for redux.
		$options[] = array(
			'title'             => $this->item_name . ' ' . __( 'License', 'shoestrap' ),
			'id'                => $this->field_name . '_key',
			'default'           => $this->default_license,
			'type'              => 'text',
			'validate_callback' => array( $this, 'license_field_callback' ),
			'description'       => $this->description
		);

		return $options;

	}

	function setup_updater() {
		// Setup the updater

		if ( $this->mode == 'theme' ) {
			// Themes updater

			$edd_updater = new EDD_SL_Theme_Updater(
				array(
					'remote_api_url'  => $this->remote_api_url,
					'version'         => $this->version,
					'license'         => $this->license,
					'item_name'       => $this->item_name,
					'author'          => $this->author
				)
			);

		} else {
			// Plugins updater

			$edd_updater = new EDD_SL_Plugin_Updater(
				$this->remote_api_url,
				$this->file,
				array(
					'version'   => $this->version,
					'license'   => $this->license,
					'item_name' => $this->item_name,
					'author'    => $this->author
				)
			);

		}

	}

	/*
	 * When a new key is entered, make sure that the license status is reset.
	 * We're setting this up as a custom validation callback function for redux.
	 */
	function license_field_callback( $field, $value, $existing_value ) {

		if( $existing_value && $existing_value != $value ) {

			// Delete the transient.
			delete_transient( $this->field_name . '_status' );

		}

		$return['value'] = $value;

		return $return;
	}


	/*
	 * Activate the license
	 */
	function activate_license() {
		global $wp_version;
		$ss_settings = get_option( 'shoestrap' );

		// Only continue if the status transient is not set to 'valid'.
		if ( get_transient( $this->field_name . '_status' ) == 'valid' ) {
			return;
		}

		// data to send in our API request
		$api_params = array(
			'edd_action'=> 'activate_license',
			'license' 	=> $this->license,
			'item_name' => urlencode( $this->item_name )
		);

		// Call the custom API.
		$response = wp_remote_get(
			add_query_arg( $api_params, $this->remote_api_url ),
			array( 'timeout' => 15, 'sslverify' => false )
		);

		// make sure the response came back okay
		if ( is_wp_error( $response ) ) {
			return false;
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// Set the transient for 6 hours.
		set_transient( $this->field_name . '_status', $license_data->license, 6 * 60 * 60 );
	}

	/*
	 * De-activate the license
	 */
	function deactivate_license() {
		global $wp_version;
		$ss_settings = get_option( 'shoestrap' );

		// data to send in our API request
		$api_params = array(
			'edd_action'=> 'deactivate_license',
			'license' 	=> $this->license,
			'item_name' => urlencode( $this->item_name )
		);

		// Call the custom API.
		$response = wp_remote_get(
			add_query_arg( $api_params, $this->remote_api_url ),
			array( 'timeout' => 15, 'sslverify' => false )
		);

		// make sure the response came back okay
		if ( is_wp_error( $response ) ) {
			return false;
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// Set the transient for 6 hours.
		delete_transient( $this->field_name . '_status' );
	}
}