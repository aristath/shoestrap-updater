<?php

class SS_Updater {

	private static $instance;

	private function __construct() {

		// Load the plugin updater class
		// This class is provided by Easy Digital Downloads Software Licensing.
		// More info can be found here: https://easydigitaldownloads.com/extensions/software-licensing/
		if( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {
			include_once( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
		}

		// Load the theme updater class
		// This class is provided by Easy Digital Downloads Software Licensing.
		// More info can be found here: https://easydigitaldownloads.com/extensions/software-licensing/
		if( ! class_exists( 'EDD_SL_Theme_Updater' ) ) {
			include_once( dirname( __FILE__ ) . '/EDD_SL_Theme_Updater.php' );
		}

		// Add our custom plugin headers
		add_filter( 'extra_plugin_headers', array( $this, 'file_header_license_info' ) );
		// Detect plugins that will use our updater
		add_action( 'plugins_loaded', array( $this, 'detect_plugins' ) );
		// Detect themes that will use our updater
		add_action( 'plugins_loaded', array( $this, 'detect_themes' ) );

		// Add licensing section to redux
		add_filter( 'redux/options/shoestrap/sections', array( $this, 'licensing' ), 999 );

		// Add the updater fields.
		do_action( 'shoestrap_updater_init' );
	}

	/**
	 * Get instance function.
	 * Make sure only one instance of this class is present.
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new self;
		}

	}

	/*
	 * Create the licensing section
	 */
	function licensing( $sections ) {
		global $redux;

		// Blog Options
		$section = array(
			'title' => __( 'Licensing & Updates', 'shoestrap' ),
			'icon'  => 'el-icon-laptop',
		);


		$fields[] = array();

		$section['fields'] = apply_filters( 'shoestrap_licensing_options_modifier', $fields );

		$sections[] = $section;

		return $sections;
	}

	/*
	 * Retrieve our custom headers.
	 * These will be used by the updater later on to detect plugin properties.
	 */
	function file_header_license_info( $headers ) {
	    $headers[] = 'Software Licensing';
		$headers[] = 'Software Licensing Description';
		$headers[] = 'Software Licensing URL';

		return $headers;
	}

	/*
	 * Detects any plugins that have the appropriate header
	 * then initializes an updater for each plugin that needs it.
	 * Uses the header information defined in file_header_license_info()
	 */
	function detect_plugins() {
		$ss_settings = get_option( 'shoestrap' );

		// make sure that the 'get_plugins' function exists
		if ( ! function_exists( 'get_plugins' ) ) {

			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		}

		// Get an array of all the plugins
		$plugins = get_plugins();

		foreach ( $plugins as $plugin => $headers ) {

			// Process the updater if the "Software Licensing" header is NOT empty
			if ( ! empty( $headers['Software Licensing'] ) ) {

				$field_name = preg_replace( '/[^a-zA-Z0-9_\s]/', '', str_replace( ' ', '_', strtolower( $headers['Name'] ) ) );
				$license_option = $ss_settings[$field_name . '_key'];

				// Get the description
				if ( ! empty( $headers['Software Licensing Description'] ) ) {
					$description = $headers['Software Licensing Description'];
				} else {
					$description = '';
				}

				// Get the Remote API URL.
				//If one is specified then use that, otherwise use the default.
				if ( ! empty( $headers['Software Licensing URL'] ) ) {
					$remote_api_url = $headers['Software Licensing URL'];
				}


				// If 'Software Licensing' is set to something other than 'true',
				// assume that a default license is specified.
				if ( ! empty( $headers['Software Licensing'] ) && $headers['Software Licensing'] != 'true' ) {
					$default_license = $headers['Software Licensing'];
				} else {
					$default_license = null;
				}


				// Get the license key to use.
				if ( isset( $license_option ) && ! empty( $license_option ) ) {
					// A license is entered in the options field
					$license = trim( $license_option );
				} else {
					// Do we have a default license specified?
					if ( isset( $default_license ) ) {
						$license = $default_license;
					} else {
						// No license was found.
						$license = null;
					}
				}

				// Get the author.
				$author  = sanitize_text_field( $headers['Author'] );

				// Populate the updater arguments
				$args = array(
					'file'            => WP_PLUGIN_DIR . '/' . $plugin,
					'item_name'       => $headers['Name'],
					'license'         => $license,
					'version'         => $headers['Version'],
					'author'          => $author,
					'mode'            => 'plugin',
					'title'           => $headers['Name'],
					'field_name'      => $field_name,
					'description'     => $description,
					'default_license' => $default_license
				);

				// If an API URL has been specified, override the default here.
				if ( isset( $remote_api_url ) ) {

					$args['remote_api_url'] = $remote_api_url;

				}

				// Run the updater
				$updater = new SS_EDD_SL_Updater( $args );
			}
		}
	}

	/*
	 * Detects any themes that have the appropriate headers
	 * then initializes an updater for each plugin that needs it.
	 * Uses the header information defined in file_header_license_info()
	 */
	function detect_themes() {
		$ss_settings = get_option( 'shoestrap' );

		// make sure that the 'get_plugins' function exists
		if ( ! function_exists( 'wp_get_themes' ) ) {

			require_once( ABSPATH . 'wp-admin/includes/theme.php' );

		}

		// Get an array of all the plugins
		$themes      = wp_get_themes();
		$themes_root = get_theme_root();

		// The list of custom headers we're using
		$extra_headers = array(
			'Software Licensing'             => 'Software Licensing',
			'Software Licensing Description' => 'Software Licensing Description',
			'Software Licensing URL'         => 'Software Licensing URL',
		);

		foreach ( $themes as $theme => $headers ) {

			$theme_file     = $themes_root . '/' . $theme . '/style.css';
			$file_headers   = get_file_data( $theme_file, $extra_headers );
			$field_name     = preg_replace( '/[^a-zA-Z0-9_\s]/', '', str_replace( ' ', '_', strtolower( $headers['Name'] ) ) );

			// Process the updater if the "Software Licensing" header is NOT empty
			if ( ! empty( $file_headers['Software Licensing'] ) ) {

				// Get the description
				if ( ! empty( $file_headers['Software Licensing Description'] ) ) {
					$description = $file_headers['Software Licensing Description'];
				} else {
					$description = '';
				}

				// If 'Software Licensing' is set to something other than 'true',
				// assume that a default license is specified.
				if ( $headers['Software Licensing'] != 'true' ) {
					$default_license = $file_headers['Software Licensing'];
				}

				// Get the Remote API URL.
				//If one is specified then use that, otherwise use the default.
				if ( ! empty( $file_headers['Software Licensing URL'] ) ) {
					$remote_api_url = $file_headers['Software Licensing URL'];
				}

				// Get the license key to use.
				if ( isset( $ss_settings[$field_name . '_key'] ) && ! empty( $ss_settings[$field_name . '_key'] ) ) {
					// A license is entered in the options field
					$license = trim( $ss_settings[$field_name . '_key'] );
				} else {
					// Do we have a default license specified?
					if ( isset( $default_license ) ) {
						$license = $default_license;
					} else {
						// No license was found.
						$license = null;
					}
				}

				// Sanitize the authors fiels (get rid of the link to the site).
				$author  = sanitize_text_field( $headers['Author'] );

				// Populate the updater arguments
				$args = array(
					'file'            => $theme_file,
					'item_name'       => $headers['Name'],
					'license'         => $license,
					'version'         => $headers['Version'],
					'author'          => $author,
					'mode'            => 'theme',
					'title'           => $headers['Name'],
					'field_name'      => $field_name,
					'description'     => $description,
					'default_license' => $default_license
				);

				// If an API URL has been specified, override the default here.
				if ( isset( $remote_api_url ) ) {

					$args['remote_api_url'] = $remote_api_url;

				}

				// Run the updater
				$updater = new SS_EDD_SL_Updater( $args );
			}
		}
	}
}
