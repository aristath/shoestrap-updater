<?php
/*
Plugin Name: Shoestrap Licensing & Updates
Plugin URI: http://wpmu.io
Description: Licenses and updates for themes and plugins using EDD-SL.
Version: 0.1
Author: Aristeides Stathopoulos
Author URI:  http://aristeides.com
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load files
require_once( dirname( __FILE__ ) . '/includes/class-SS_Updater.php' );
require_once( dirname( __FILE__ ) . '/includes/class-SS_EDD_SL_Updater.php' );

SS_Updater::get_instance();